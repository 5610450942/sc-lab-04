package view;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUI extends JFrame{
	
	private JPanel panel = new JPanel(null);
	private JTextArea txt1 = new JTextArea("Nested Loop\n--------------------------\n");
	private JTextField txt2 = new JTextField();
	private JComboBox box = new JComboBox();
	private JButton bt1 = new JButton("run");
	private JButton bt2 = new JButton("clear sceen");
	
	
	
	
	
	public GUI(){
		createFrame();
	}
	
	public void createFrame(){
		
		setBounds(340,130,600,260);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Nested Loop");
		
		setLayout(null);
		
		panel.setBounds(0,0,580,220);
		txt1.setBounds(8,8,300,207);
		txt2.setBounds(320,48,250,30);
		box.setBounds(320,8,250,30);
		bt1.setBounds(320,88,250,30);
		bt2.setBounds(320,178,250,30);
		
		
		
		add(panel);
		panel.add(txt1); 
		panel.add(txt2); 
		panel.add(box);
		panel.add(bt1); panel.add(bt2);
		
		setVisible(true);
		
	}
	
	public JComboBox getBox(){
		return box;
	}
	
	public JTextArea getTxT1(){
		return txt1;
	}
	
	public JTextField getTxT2(){
		return txt2;
	}
	
	public JButton getButton1(){
		return bt1;
	}
	
	public JButton getButton2(){
		return bt2;
	}
	

}
