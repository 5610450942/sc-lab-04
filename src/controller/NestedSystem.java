package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.NestedLoop;
import model.NestedLoop1;
import model.NestedLoop2;
import model.NestedLoop3;
import model.NestedLoop4;
import model.NestedLoop5;
import view.GUI;

public class NestedSystem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		NestedSystem controller = new NestedSystem();
		GUI view = new GUI();
		
		NestedLoop1 nest1 = new NestedLoop1();
		NestedLoop2 nest2 = new NestedLoop2();
		NestedLoop3 nest3 = new NestedLoop3();
		NestedLoop4 nest4 = new NestedLoop4();
		NestedLoop5 nest5 = new NestedLoop5();
		
		controller.setModel(nest1,nest2,nest3,nest4,nest5);
		controller.setView(view);
		controller.addBox();
		controller.setNestedListener();

	}
	
	public void setModel(NestedLoop nest1,NestedLoop nest2,NestedLoop nest3,NestedLoop nest4,
			NestedLoop nest5){
		this.nest1 = nest1;
		this.nest2 = nest2;
		this.nest3 = nest3;
		this.nest4 = nest4;
		this.nest5 = nest5;
	}
	
	public void setView(GUI view){
		this.view = view;
	}
	
	public void addBox(){
		view.getBox().addItem(nest1);
		view.getBox().addItem(nest2);
		view.getBox().addItem(nest3);
		view.getBox().addItem(nest4);
		view.getBox().addItem(nest5);
	}
	
	public void setNestedListener(){
		//bt1 listener
		view.getButton1().addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				int n = Integer.parseInt(view.getTxT2().getText());
				NestedLoop nested = (NestedLoop) view.getBox().getSelectedItem();
				view.getTxT1().setText(view.getTxT1().getText()+nested.getNested(n)+"\n");
			}
		});
		
		//bt2 listener
		view.getButton2().addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
				view.getTxT1().setText("Nested Loop\n--------------------------\n");
			}
		});
		
	}
	
	
	
	
	GUI view;
	NestedLoop nest1;
	NestedLoop nest2;
	NestedLoop nest3;
	NestedLoop nest4;
	NestedLoop nest5;
}
