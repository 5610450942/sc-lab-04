package model;

public interface NestedLoop {
	
	public String getNested(int n);
	
	public String toString();

}
