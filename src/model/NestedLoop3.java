package model;

public class NestedLoop3 implements NestedLoop{
	
	public String getNested(int n){
		String nested = "";
		for(int i=1;i<=n;i++){
			for(int j=1;j<=i;j++){
				nested += "*";
			}
			nested += "\n";
		} return nested;
	}
	
	public String toString(){
		return "Nested Loop3";
	}


}
