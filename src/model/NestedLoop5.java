package model;

public class NestedLoop5 implements NestedLoop{

	public String getNested(int n){
		String nested = "";
		for(int i=1;i<=n;i++){
			for(int j=1;j<=5;j++){
				if((i+j)%2==0){
					nested += "*";
				}else{
					nested += " ";
				}
				
			}
			nested += "\n";
		} return nested;
	}
	
	public String toString(){
		return "Nested Loop5";
	}

}
