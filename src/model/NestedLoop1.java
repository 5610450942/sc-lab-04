package model;

public class NestedLoop1 implements NestedLoop{
	
	public String getNested(int n){
		String nested = "";
		for(int i=1;i<=n-1;i++){
			for(int j=1;j<=n;j++){
				nested += "*";
			}
			nested += "\n";
		} return nested;
	}
	
	public String toString(){
		return "Nested Loop1";
	}

	

}
