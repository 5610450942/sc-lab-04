package model;

public class NestedLoop4 implements NestedLoop{
	
	public String getNested(int n){
		String nested = "";
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++){
				if(j%2==0){
					nested += "*";
				}else{
					nested += "_";
				}
				
			}
			nested += "\n";
		} return nested;
	}
	
	public String toString(){
		return "Nested Loop4";
	}


}
